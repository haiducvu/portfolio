/* eslint-disable no-unused-vars */
// Hàm filter()=> dùng để tìm các các object trong 1 mảng=> kết quả trả về 1 mảng có chứa object
let mangSanPham=[
  {MaSP:1, TenSP: 'sony Xperia1', Gia: 1000, HangSX: 'SONY1' },
  {MaSP:2, TenSP: 'sony Xperia2', Gia: 12000, HangSX: 'SONY2' },
  {MaSP:3, TenSP: 'sony Xperia3', Gia: 10300, HangSX: 'SONY3' },
  {MaSP:4, TenSP: 'sony Xperia4', Gia: 103400, HangSX: 'SONY4' },
  {MaSP:5, TenSP: 'sony Xperia5', Gia: 156000, HangSX: 'SONY5' },
  {MaSP:6, TenSP: 'sony Xperia6', Gia: 10700, HangSX: 'SONY6' },
  {MaSP:7, TenSP: 'sony Xperia7', Gia: 10800, HangSX: 'SONY7' },
]

const result= mangSanPham.filter( acc=> acc.MaSP === 6);
console.log ('Hàm filter-Result',result);

//Hàm find() => dùng để tìm các thuộc tính của object trong 1 mảng=> kết quả trả về 1 object cần tìm
const resultFind=mangSanPham.find(acc=>acc.MaSP=== 7);
console.log('Hàm find-Result',resultFind);

//Hàm for each() Hàm xử lý mảng=> dùng để duyệt mảng làm công việc gì đó, giống for
const resultForEach= mangSanPham.forEach((acc,index)=>{
  console.log(acc);
  return acc;
})
console.log('Result Each',resultForEach);

//Hàm map so với for each lấy hết giá trị kể cả define

//Hàm reduce thực thi n lần so với n phần tử của mảng nhằm tạo ra 1 giá trị mới( có thể là 1 biến, 1 mảng, 1 object... tùy theo return trong hàm)  
const resultReduce= mangSanPham.reduce((output,item, index)=>{
  output+=item.Gia;
  return output;
},0); //defaultOutput output= 0

const resultReduce2= mangSanPham.reduce((output,item, index)=>{
  if(item.TenSP === 'sony Xperia5')
  {
    output.push(item);
  }
  return output;
},[]); //defaultOutput output= 0
console.log('ResultReduce2',resultReduce2);